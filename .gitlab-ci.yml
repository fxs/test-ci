---
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/lint.gitlab-ci.yml'
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/image-factory.gitlab-ci.yml'

variables:
  DOCKER_REGISTRY_IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA
  DOCKER_DOCKERHUB_IMAGE_REPOSITORY: index.docker.io/jfxs/test-ci
  DOCKER_DOCKERHUB_RM_URL: jfxs/test-ci
  DOCKER_QUAY_IMAGE_REPOSITORY: quay.io/ifxs/test-ci
  DOCKER_QUAY_RM_URL: quay.io/ifxs/test-ci
  DOCKER_SBOM_SUBSET: "^github.com/go-task/task/v3 |^ca-certificates |^curl |^file |^git |^jq |^ncurses "
  LINT_DOCKER: "true"
  LINT_SHELL: "true"
  LINT_SHELL_FILE: "files/*.sh"
  TASKFILE_VERSION: .Taskfile-version.yml

stages:
  - lint
  - build-image
  - test-image
  - trigger-publish-image
  - publish-image
#  - notify-gchat

docker:build:
  before_script:
    - if [ ! -f "$TASKFILE_VERSION" ]; then wget -q -O "$TASKFILE_VERSION" https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/version.yml; fi
    - DOCKER_BUILD_VERSION=$(task --taskfile "$TASKFILE_VERSION" get-latest-github REPO=go-task/task)

sanity-test:amd64:
  image: $DOCKER_REGISTRY_IMAGE_TAG
  stage: test-image
  script:
    - task --version

sanity-test:arm64:
  extends: sanity-test:amd64
  tags:
    - arm64

sanity-test:version:
  before_script:
    - if [ ! -f "$TASKFILE_VERSION" ]; then wget -q -O "$TASKFILE_VERSION" https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/version.yml; fi
    - DOCKER_VERSION_CLI=$(task --taskfile "$TASKFILE_VERSION" get-docker-cli-version IMG="$DOCKER_REGISTRY_IMAGE_TAG" CMD="task --version")

publish:dockerhub:
  before_script:
    - if [ "$PUBLISH" = "1" ]; then echo -n "$REGISTRY_PASS" | docker login -u "$REGISTRY_USER" --password-stdin; fi
  script:
    - !reference [.snippets, prerequisites-tools]
    - !reference [.snippets, prerequisites-task]
    - !reference [.snippets, check-new-image]
    # Update README
    - sed -i'.bu' "s=--VERSION--=$TAG_FULL_NEW, $TAG_MAJOR_MINOR_NEW, $TAG_MAJOR_NEW=g" README.md
    - task --taskfile "$DOCKER_TASKFILE_SBOM" get-sbom-subset INPUT=sbom_new.txt OUTPUT=sbom_subset.txt GREP="$DOCKER_SBOM_SUBSET"
    - task --taskfile "$DOCKER_TASKFILE_SBOM" set-sbom-in-file SBOM=sbom_subset.txt
    # Publish, sign, sbom full version
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:$TAG_FULL_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then cosign sign --key "$COSIGN_PRIVATE_KEY" "$REGISTRY_URL:$TAG_FULL_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then task --taskfile "$DOCKER_TASKFILE_SBOM" attach-sbom-attest I="$REGISTRY_URL:$TAG_FULL_NEW" K="$COSIGN_PRIVATE_KEY"; fi
    # Publish major and major-minor latest tag
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:$TAG_MAJOR_MINOR_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:$TAG_MAJOR_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:latest"; fi
    # Publish README
    - if [ "$PUBLISH" = "1" ]; then docker pushrm "$REGISTRY_RM_URL"; fi

publish:quay:
  extends: publish:dockerhub
  variables:
    REGISTRY_USER: $QUAY_USER
    REGISTRY_PASS: $QUAY_PASSWORD
    REGISTRY_URL: $DOCKER_QUAY_IMAGE_REPOSITORY
    REGISTRY_RM_URL: $DOCKER_QUAY_RM_URL
  before_script:
    - if [ "$PUBLISH" = "1" ]; then echo -n "$REGISTRY_PASS" | docker login -u "$REGISTRY_USER" --password-stdin quay.io; fi
  needs:
    - docker:build
    - trigger:publish
    - publish:dockerhub
