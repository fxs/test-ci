
DOCKER = docker

IMAGE_NODE = node:lts-buster

## nodejs modules management
## -------------------------
yarn-install: ## Install nodejs modules with yarn. Arguments: [pull=n]
yarn-install:
	if ! [ -x "$(command -v yarn)" ]; then \
		if [ -z ${pull} ]; then $(DOCKER) pull ${IMAGE_NODE} ; fi && \
		$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache install ; \
	else \
		yarn install ; \
	fi

yarn-outdated: ## Check outdated npm packages. Arguments: [pull=n]
yarn-outdated:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache outdated || true

yarn-upgrade: ## Upgrade packages. Arguments: [pull=n]
yarn-upgrade:
	@if [ -z ${pull} ]; then \
		$(DOCKER) pull ${IMAGE_NODE} ; \
	fi
	$(DOCKER) run -t --rm -v ${PWD}:/root -w /root ${IMAGE_NODE} yarn --cache-folder /root/.node_cache upgrade

clean-node-modules: ## Remove node_modules directory
clean-node-modules:
	rm -rf .node_cache
	rm -rf node_modules

.PHONY: yarn-install yarn-outdated yarn-upgrade clean-node-modules

## Go
## ------
clean-mod: ## Clean dependencies
clean-mod:
	@go mod tidy

get-mod: ## Download dependencies
get-mod:
	@go mod download

check-mod-outdated: ## Check outdated dependencies
check-mod-outdated:
	@go list -u -f '{{if (and (not (or .Main .Indirect)) .Update)}}{{.Path}}: {{.Version}} -> {{.Update.Version}}{{end}}' -m all 2> /dev/null

check-format: ## Check formated code
check-format:
	@if [ -n "$(gofmt -l .)" ]; then \
        echo "Go code is not formatted:" && \
        gofmt -d . && \
        exit 1; \
	fi
	go vet ./...

build-local: ## Build for local architecture
build-local:
	env GOOS=linux GOARCH=amd64 go build -ldflags="-X 'main.version=LOCAL' -s -w" -o hello_local_linux_amd64 ./cmd/hello
	env GOOS=darwin GOARCH=amd64 go build -ldflags="-X 'main.version=LOCAL' -s -w" -o hello_local_darwin_amd64 ./cmd/hello

build-version: ## Build with defined version. Arguments: version=1.3.2
build-version:
	test -n "${version}"  # Failed if version not set
	mkdir -p bin
	mkdir -p dist
	env GOOS=linux GOARCH=amd64 go build -ldflags="-X 'main.version=${version}' -s -w" -o bin/hello_${version}_linux_amd64 ./cmd/hello
	env GOOS=darwin GOARCH=amd64 go build -ldflags="-X 'main.version=${version}' -s -w" -o bin/hello_${version}_darwin_amd64 ./cmd/hello
	for i in bin/*; do tar -czf $$i.tar.gz $$i; done
	cp bin/*.tar.gz dist/

PHONY: clean-mod get-mod check-mod-outdated build-local build-version

## Tests
## ------
unit-test: ## Run unit test
unit-test:
	@go test ./... -cover

unit-test-verbose: ## Run unit test with details
unit-test-verbose:
	@go test ./... -v -cover | sed ''/PASS/s//$(printf "\033[32mPASS\033[0m")/'' | sed ''/FAIL/s//$(printf "\033[31mFAIL\033[0m")/''

PHONY: unit-test unit-test-verbose

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
