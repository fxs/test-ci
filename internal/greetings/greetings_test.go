package greetings

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHello(t *testing.T) {
	s := Hello("test")
	assert.Equal(t, "Hello, test!", s)
}
