package main

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(t *testing.T) {
	os.Args = []string{"./hello"}
	out = bytes.NewBuffer(nil)
	main()
	assert.Equal(t, "hello version DEV\nHello, World!\n", out.(*bytes.Buffer).String())
}
