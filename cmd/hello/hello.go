package main

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/fxs/test-ci/internal/greetings"
)

// Version Version of run
var version string = "DEV"

// out Default output
var out io.Writer = os.Stdout

func main() {
	fmt.Fprintf(out, "hello version %s\n", version)
	message := greetings.Hello("World")
	fmt.Fprintln(out, message)
}
