# jfxs / test-ci

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/test-ci/badges/master/pipeline.svg)](https://gitlab.com/fxs/test-ci/pipelines)

A test project to develop CI/CD pipeline for tests projects:

A [cntlm](https://cntlm.sourceforge.net/) Docker image:

* **lightweight** image based on Alpine Linux,
* multiarch with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* an **SBOM attestation** added using [Syft](https://github.com/anchore/syft).

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/alpine-cntlm) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/alpine-cntlm) The Docker Hub registry.

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/fxs/test-ci/-/blob/master/Dockerfile) and contains:

--SBOM-TABLE--

Details are updated on [Dockerhub Overview page](https://hub.docker.com/r/jfxs/test-ci) when an image is published.

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available [here](https://gitlab.com/op_so/docker/cosign-public-key)
To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the SBOM attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | .payload | fromjson | .predicate'
```

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/test-ci/blob/master/LICENSE) file for details.
